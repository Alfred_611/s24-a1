// step 2
db.users.find({  $or: [{ "firstName": "s"},{"lastName": "d"}] }{"_id": 0});



// step 3
db.users.find({
	$and: [
	{department: "HR"},
	{age: {$gte: 70}}
	]
});



// step 4
db.users.find({
	$and: [
	{ "firstName": { $regex: "e", $options: "$i"}
	},
	{"age": {$lte: 30}
	}
	]
});